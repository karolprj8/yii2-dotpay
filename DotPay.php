<?php
/**
 * File DotPay.php.
 *
 * 
 */

namespace karolprj8;

use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\base\Component;


use twofox\goods\models\Goods;
use yii\helpers\Url;
use frontend\modules\payments\models\Payments;
use backend\modules\ticket\models\Vouchers;


class DotPay extends Component {
    
    const CLASSNAME = 'DotPay';

    //region Log levels
    /*
     * Logging level can be one of FINE, INFO, WARN or ERROR.
     * Logging is most verbose in the 'FINE' level and decreases as you proceed towards ERROR.
     */
    const LOG_LEVEL_FINE = 'FINE';
    const LOG_LEVEL_INFO = 'INFO';
    const LOG_LEVEL_WARN = 'WARN';
    const LOG_LEVEL_ERROR = 'ERROR';
    //endregion

    //region API settings
    public $clientId;
    public $clientSecret;
    public $isSandbox = true;
    public $currency = 'PLN';
    public $dotpay_url = 'https://ssl.dotpay.pl/t2/';
    //public $dotpay_url = 'https://ssl.dotpay.pl/test_payment/';
    public $config = [];
    public $thx_url;
    public $thx_urlc = 'http://dreamjump.pl/pl/payments/dotpay';	
	public $allow_server = array('217.17.41.5', '195.150.9.37', '81.219.241.115');

    /** @var ApiContext */
    private $_apiContext = null;

    public function createPayments($data = [], $fastorderemail=null) {
        //print_r($data);

        $total = 0;
        $st = 0;

        $items = [];
        foreach ($data as $value) {

            if (!$goods = Goods::findOne($value['id']))
                continue;
            
            if($st==0){
                $pay_id = $goods -> local -> payments["dotpay"];
                $this->clientId = Yii::$app->params['payments']["dotpay"]['accounts'][$pay_id]['clientId'];
                $this->clientSecret = Yii::$app->params['payments']["dotpay"]['accounts'][$pay_id]['clientSecret'];
            }
            
            $count = max(1, intval(@$value['count']));
            $total += $goods -> getPrice() * $count;
            $good_id = str_replace('-', '', substr($goods->local->slug, 0 ,5)); 

            if (isset($value['products'])) {
                $Products = $goods -> getProd();
                foreach ($value['products'] as $product) {
                    if (!isset($Products[$product['id']]))
                        continue;
                    
                    $count = max(1, intval(@$product['count']));
                    $product = $Products[$product['id']];
                    $total += $product -> getPrice() * $count;
                }
            }
        }
		
		//if (Yii::$app -> user -> id == 61) $total = "1";
		
		$inpayment = new Payments();
        $inpayment -> payment_id = 'DP-'.time();
        $inpayment -> gateway = self::CLASSNAME;
		$inpayment -> gateway_name = Yii::$app->params['payments']["dotpay"]['accounts'][$pay_id]['label'];
            
        if(\Yii::$app->user->isGuest)
            $inpayment -> email = $fastorderemail;
        else
            $inpayment -> user_id = Yii::$app -> user -> id;
            
        $inpayment -> amount = $total;
        $inpayment -> currency = $goods -> currency;
        $inpayment -> cart = $data;
		// 
		
		
        if ($inpayment -> save())
		{
			$inpayment -> cart = $data;
            $inpayment -> payment_id  = 'DP'.$inpayment->id;
			$inpayment -> save();
			
			$url = Yii::$app->params['payments']["dotpay"]['accounts'][$pay_id]['url'];
			$id = $this->clientId;
			$lang = Yii::$app->language;
			$fname = Yii::$app->user->isGuest ? '' : Yii::$app -> user -> identity -> profile -> name;
			$lname = Yii::$app->user->isGuest ? '' : Yii::$app -> user -> identity -> profile -> surname;
			$email = Yii::$app->user->isGuest ? $fastorderemail : Yii::$app -> user -> identity -> email;
            
			$form ="<form id='dotpayform' name='dotpayform' method='POST'  action='{$url}'>
			<input type='hidden' name='api_version' value='dev' />
			<input type='hidden' name='id' value='{$id}' />
			<input type='hidden' name='lang' value='{$lang}' />
			<input type='hidden' name='firstname' value='{$fname}' />
			<input type='hidden' name='lastname' value='{$lname}' />
			<input type='hidden' name='email' value='{$email}' />
			<input type='hidden' name='description' value='DreamJump payment: {$inpayment -> payment_id}' />
			<input type='hidden' name='amount' value='{$total}' />
			<input type='hidden' name='currency' value='{$inpayment -> currency}' />
			<input type='hidden' name='control' value='{$inpayment -> payment_id}' />
			<input type='hidden' name='typ' value='3' />
			<input type='hidden' name='URL' value='".Url::to(['/payments/success', 'payment' => 'dotpay'], true)."' />
			<input type='hidden' name='URLC' value='".Url::to(['/payments/dotpay'], true)."' />
			</form>"; 
			return array(0=>'dotpay', 1=>$form);
		}
			
        exit ;

        
    }

    public function checkPayments() {
		
		
		$error = "";
		
		
		//Sprawdzamy czy w/w tablica zawiera numer IP klienta który właśnie się z nami łączy
		if (!in_array(Yii::$app->request->userIP, $this->allow_server)) {
				return "Błąd! IP: ".Yii::$app->request->userIP;
		}
		 
		 
		 
		//Jeżeli wszystko jest OK, to zaczynamy księgowanie
		if (!isset($_POST['operation_status']) || !isset($_POST['operation_original_amount']) || !isset($_POST['control']) || !isset($_POST['operation_original_currency']) || !isset($_POST['id']) || !isset($_POST['signature']))
		{
			if (!isset($_POST['t_status']) || !isset($_POST['orginal_amount']) || !isset($_POST['control']) || !isset($_POST['id']) || !isset($_POST['md5']))
			{
				return "Błąd! Niepełna tablica post.";
			}
		}		
		
		
		if (array_key_exists('operation_status', $_POST) || array_key_exists('t_status', $_POST))
		{
			
			$pin = $this->clientSecret;
			
			$status = array_key_exists('operation_status', $_POST) ? $_POST['operation_status'] : $_POST['t_status'];
			$status = ($status == 'completed' ? 2 : $status);
			if ($status == '1') return "OK";
			if (array_key_exists('operation_original_amount', $_POST)) 
			{
				$amount = trim($_POST['operation_original_amount']);
				$currency = trim($_POST['operation_original_currency']);
				$sign = Yii::$app->request->post('id', ''). Yii::$app->request->post('operation_number', ''). Yii::$app->request->post('operation_type', ''). Yii::$app->request->post('operation_status', ''). Yii::$app->request->post('operation_amount', ''). Yii::$app->request->post('operation_currency', ''). Yii::$app->request->post('operation_withdrawal_amount', ''). Yii::$app->request->post('operation_commission_amount', ''). Yii::$app->request->post('operation_original_amount', ''). Yii::$app->request->post('operation_original_currency', ''). Yii::$app->request->post('operation_datetime', ''). Yii::$app->request->post('operation_related_number', ''). Yii::$app->request->post('control', ''). Yii::$app->request->post('description', ''). Yii::$app->request->post('email', ''). Yii::$app->request->post('p_info', ''). Yii::$app->request->post('p_email', ''). Yii::$app->request->post('channel', ''). Yii::$app->request->post('channel_country', ''). Yii::$app->request->post('geoip_country', '');
			} else {
				
				$tmp = trim($_POST['orginal_amount']);
				$tmp = explode(' ', $tmp);
				$amount = $tmp[0];
				$currency = $tmp[1];
				
				$sign = ":". Yii::$app->request->post('id', '').":".  Yii::$app->request->post('control', '').":". Yii::$app->request->post('t_id', '').":".  Yii::$app->request->post('amount', '').":".  Yii::$app->request->post('email', '').":".  Yii::$app->request->post('service', '').":".  Yii::$app->request->post('code', '').":".  Yii::$app->request->post('username', '').":".  Yii::$app->request->post('password', '').":".  Yii::$app->request->post('t_status', '');
				
			}
			$control = $_POST['control'];
			$paymentId = trim($_POST['control']);
			$pin = $this->clientSecret;
		
			if ($status == 2 && $amount != '' && $control != '') {
				
				$paymentId = trim($_POST['control']);
				$amount = trim($_POST['operation_original_amount']);
				
				
				if ($paymentId!=null && is_numeric($amount)) {
					if (!$data = Payments::find() -> where(['gateway' => self::CLASSNAME, 'payment_id' => $paymentId, 'status' => Payments::NOTVERIFIED]) -> one())
					{
						if ($data2 = Payments::find() -> where(['gateway' => self::CLASSNAME, 'payment_id' => $paymentId]) -> one())
						{
							if ($data2->status == 1) return "OK";
						}
						return "Błąd! Data is null: 'gateway'->'". self::CLASSNAME."'; 'payment_id'->'". $paymentId."';  'status'->'". Payments::NOTVERIFIED."'";
					}
					
					$goods_a = [];
					$total  = 0;
					$st = 0;
					foreach ($data->cart as $value) {
						if (!$goods = Goods::findOne($value['id']))
							continue;
						
						if($st==0){
							$pay_id = $goods -> local -> payments["dotpay"];
							$this->clientId = Yii::$app->params['payments']["dotpay"]['accounts'][$pay_id]['clientId'];
							$this->clientSecret = Yii::$app->params['payments']["dotpay"]['accounts'][$pay_id]['clientSecret'];
							
							
							if (array_key_exists('operation_original_amount', $_POST)) 
							{
								$sign = $this->clientSecret.$sign;
								$signature=hash('sha256', $sign);
								$post_signature = $_POST['signature'];
							} else {
								$sign = $this->clientSecret.$sign;
								$signature=md5($sign);
								$post_signature = $_POST['md5'];
							}
							
							if ($post_signature != $signature)
								return "Błąd! Błąd podpisu: ".$signature;
						}
				
						$count = max(1, intval(@$value['count']));
                        $total += $goods -> getPrice() * $count; 
                        
						$goods_a[$value['id']] = ['id'=>$value['id'], 'count'=>$count, 'total'=>$goods -> getPrice(), 'currency'=>$goods -> currency, 'goods_prod'=>[], 'products'=>[]];
						
						 if (isset($value['products'])) {
							$Products = $goods -> getProd();

							foreach ($value['products'] as $product) {
								if (!isset($Products[$product['id']]))
									continue;
                         
                                $count = max(1, intval(@$product['count']));
                                $goods_a[$value['id']]['products'][$product['id']] = $product;
								$product = $Products[$product['id']];                                       
								$goods_a[$value['id']]['total'] += $product -> getPrice() * $count;
                                $total += $product -> getPrice() * $count;
							}
						}
						
						$st++;
					}
					if ($data->amount == $amount && $data->currency == $currency)
					{
						//$data -> payer_info = $payment -> payer -> payer_info;
						$data -> status = Payments::VERIFIED;
						
						if ($data -> save()) {
                            $ct = 0;   
                            foreach ($goods_a as $value) {                    
                                $dt = ['goods' => $value['id'], 'count_goods'=>$value['count'], 'payments_id' => $data -> id, 'price' => $total, 'currency' => $value['currency'], 'goods_prod' => (count($value['products'])> 0 ? $value['products'] : []), 'user' => $data -> user_id, 'email' => $data -> email];
                    
                                if($data -> user_id>0){
                                    $dt['count_goods'] = 1;
                                    $count = $value['count'];
                                }else{                        
                                    $count = 1;
                                }
                    
                                while($ct < $count){                    
                                    if($ct > 0)
                                        unset($dt['goods_prod']);
                                    
                                    if($ct > 0 && $data -> user_id > 0 && isset($parent_voucher)){
                                        $dt['parent'] = $parent_voucher->id;
                                        $dt['price'] = 0;
                                    }
                        
                                    if($voucher = Vouchers::createVoucher($dt)){
                                        $ct++;
                                        
                                        if(!isset($parent_voucher))
                                          $parent_voucher = $voucher;
                                    }
                                }
                            }
                            return $ct > 0 ? "OK" : "Błąd! Błąd podczas generowania kuponów.";
                        } else {
							return "Błąd! Błąd zapisu.";
						}
					} else {
						return  "Błąd! Błędna kwota: ".$amount." lub waluta: ".$currency;
					}

				} else {
					return "Błąd! Błędna kwota: ".$amount." lub payment_id: ".$control;
				}
			} else {
				return "Błąd! Status: '".$status."'; amount: '".$amount."'; payment_id: '".$control."'";
			}
		}
    }

}
